#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QMessageBox>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    const char* windowTitle = "Pokeemerald TM/HM Learnset Editor";
    QMessageBox* errorMessageDialog = new QMessageBox(this);

    Ui::MainWindow *ui;
    QString decompDir;
    QStringList speciesNames;
    QStringList tmNames;
    QStringList tutorMoveNames;
    QVector<QBitArray> learnset;
    QVector<QBitArray> tutorLearnset;
    bool sidesSwapped = false;
    bool showTutorMoves = false;

    void setAllCheckboxesTo(bool value);
    void fillLists();
    void loadDecomp();
    void saveDecomp();
    void closeDecomp();

    void displayError(const QString& msg);

    bool getLearnset(int moveId, int monId);
    void setLearnset(int moveId, int monId, bool value);


private slots:
    void updateCheckboxes();
    void updateLearnset(QListWidgetItem* selectedMon);
    void swapCheckboxSides();
    void swapMoveLists();
    void checkAll();
    void uncheckAll();
};
#endif // MAINWINDOW_H
