#ifndef DECOMP_PARSER_H
#define DECOMP_PARSER_H

#include <QStringList>
#include <QFile>
#include <QTextStream>
#include <QBitArray>

QStringList readDecompSpeciesList(QString dir);
QStringList readDecompTMList(QString dir);
QVector<QBitArray> readDecompTMLearnset(QString dir, QStringList speciesList, QStringList tmList);
void saveDecompLearnset(QString dir, QStringList speciesList, QStringList tmList, QVector<QBitArray> learnset);
QVector<QBitArray> readDecompTutorLearnset(QString dir, QStringList speciesList, QStringList tutorMoves);
QStringList readDecompTutorMoves(QString dir);
void saveDecompTutorLearnset(QString dir, QStringList speciesList, QStringList moveList, QVector<QBitArray> learnset);
#endif // DECOMP_PARSER_H
