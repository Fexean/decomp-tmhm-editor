#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "decomp_parser.h"

#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowTitle(windowTitle);

    connect(ui->actionOpen_Decomp, &QAction::triggered, this, &MainWindow::loadDecomp);
    connect(ui->actionSave, &QAction::triggered, this, &MainWindow::saveDecomp);
    connect(ui->actionCheck_all, &QAction::triggered, this, &MainWindow::checkAll);
    connect(ui->actionUncheck_all, &QAction::triggered, this, &MainWindow::uncheckAll);

    connect(ui->tmList, SIGNAL(itemSelectionChanged()), this, SLOT(updateCheckboxes()));
    connect(ui->monList, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(updateLearnset(QListWidgetItem*)));
    connect(ui->swapSides, SIGNAL(clicked()), this, SLOT(swapCheckboxSides()));
    connect(ui->showTutorMoves, SIGNAL(clicked()), this, SLOT(swapMoveLists()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::displayError(const QString& msg){
    errorMessageDialog->setWindowTitle("Error");
    errorMessageDialog->setText(msg);
    errorMessageDialog->show();
}

void MainWindow::loadDecomp(){
    QFileDialog filedialog(this, "Select pokeemerald directory");
    filedialog.setFileMode(QFileDialog::FileMode::DirectoryOnly);
    decompDir = filedialog.getExistingDirectory();
    if(!decompDir.size()){
        return; //nothing selected, do nothing
    }

    closeDecomp(); //close previously opened decomp

    speciesNames = readDecompSpeciesList(decompDir);
    if(!speciesNames.size()){
        displayError("Failed to read species names.");
        return;
    }

    tmNames = readDecompTMList(decompDir);
    if(!tmNames.size()){
        displayError("Failed to read TM/HM names.");
        return;
    }

    tutorMoveNames = readDecompTutorMoves(decompDir);
    if(!tutorMoveNames.size()){
        displayError("Failed to read tutor move names.");
        return;
    }

    try {
        learnset = readDecompTMLearnset(decompDir, speciesNames, tmNames);
        tutorLearnset = readDecompTutorLearnset(decompDir, speciesNames, tutorMoveNames);
    } catch (QString msg) {
        displayError(msg);
        return;
    }

    fillLists();

}

void MainWindow::saveDecomp(){
    try {
        saveDecompLearnset(decompDir, speciesNames, tmNames, learnset);
        saveDecompTutorLearnset(decompDir, speciesNames, tutorMoveNames, tutorLearnset);
    } catch (QString msg) {
        displayError(msg);
    }
}

void MainWindow::closeDecomp(){
    QSignalBlocker blocker(ui->tmList); //prevents crash if sides are swapped
    ui->monList->clear();
    ui->tmList->clear();
    ui->actionSave->setEnabled(false);
    ui->actionCheck_all->setEnabled(false);
    ui->actionUncheck_all->setEnabled(false);
    ui->swapSides->setEnabled(false);
    ui->showTutorMoves->setEnabled(false);

    sidesSwapped = false;
}

void MainWindow::fillLists(){
    ui->swapSides->setEnabled(true);
    ui->actionSave->setEnabled(true);
    ui->actionCheck_all->setEnabled(true);
    ui->actionUncheck_all->setEnabled(true);
    ui->showTutorMoves->setEnabled(true);
    ui->monList->addItems(speciesNames);

    if(showTutorMoves){
        ui->tmList->addItems(tutorMoveNames);
    }else{
        ui->tmList->addItems(tmNames);
    }

    if(sidesSwapped){
        ui->monList->setCurrentRow(0);
    }else{
        ui->tmList->setCurrentRow(0);
    }
}

void MainWindow::updateCheckboxes(){
    //avoid triggering updateLearnset on checkbox change
    QSignalBlocker blocker(ui->monList);
    QSignalBlocker blocker2(ui->tmList);

    QListWidget* otherList = ui->tmList;
    QListWidget* checkboxList = ui->monList;
    if(sidesSwapped){
        QListWidget* tmp = checkboxList;
        checkboxList = otherList;
        otherList = tmp;
    }

    QListWidgetItem* item = 0;
    int otherSelected = otherList->selectionModel()->currentIndex().row();

    //add checkboxes to checkboxList
    for(int i = 0; i < checkboxList->count(); ++i){
        item = checkboxList->item(i);
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);

        if(sidesSwapped){
            item->setCheckState(getLearnset(i, otherSelected) ? Qt::Checked : Qt::Unchecked);
        }else{
            item->setCheckState(getLearnset(otherSelected, i) ? Qt::Checked : Qt::Unchecked);

        }
    }

    //remove checkboxes from other list
    for(int i = 0; i < otherList->count(); ++i){
        item = otherList->item(i);
        item->setData(Qt::CheckStateRole, QVariant());
    }
}

void MainWindow::updateLearnset(QListWidgetItem* checkbox){
    int tmId, monId;

    if(sidesSwapped){
        tmId = ui->tmList->row(checkbox);
        monId = ui->monList->selectionModel()->currentIndex().row();
    }else{
        tmId = ui->tmList->selectionModel()->currentIndex().row();
        monId = ui->monList->row(checkbox);
    }

    if(monId != -1 && tmId != -1){
        setLearnset(tmId, monId, checkbox->checkState() == Qt::Checked);
    }
}

void MainWindow::swapCheckboxSides(){
    sidesSwapped = !sidesSwapped;
    disconnect(ui->tmList);
    disconnect(ui->monList);

    if(sidesSwapped){
        connect(ui->monList, SIGNAL(itemSelectionChanged()), this, SLOT(updateCheckboxes()));
        connect(ui->tmList, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(updateLearnset(QListWidgetItem*)));
        ui->monList->setCurrentRow(0);
    }else{
        connect(ui->tmList, SIGNAL(itemSelectionChanged()), this, SLOT(updateCheckboxes()));
        connect(ui->monList, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(updateLearnset(QListWidgetItem*)));
        ui->tmList->setCurrentRow(0);
    }

    updateCheckboxes();
}

void MainWindow::setAllCheckboxesTo(bool value){
    int otherSelection, checkboxCount;

    if(sidesSwapped){
        otherSelection = ui->monList->selectionModel()->currentIndex().row();
        checkboxCount = ui->tmList->count();
    }else{
        otherSelection = ui->tmList->selectionModel()->currentIndex().row();
        checkboxCount = ui->monList->count();
    }

    for(int i = 0; i < checkboxCount; ++i){
        if(sidesSwapped){
            setLearnset(i,otherSelection, value);
        }else{
            setLearnset(otherSelection, i, value);
        }
    }
    updateCheckboxes();
}

void MainWindow::checkAll(){
    setAllCheckboxesTo(true);
}

void MainWindow::uncheckAll(){
    setAllCheckboxesTo(false);
}

void MainWindow::swapMoveLists(){
    QSignalBlocker blocker(ui->tmList);
    showTutorMoves = !showTutorMoves;
    ui->tmList->clear();
    if(showTutorMoves){
        ui->showTutorMoves->setText("Show TMs/HMs");
        ui->label->setText("Tutor Moves");
        ui->tmList->addItems(tutorMoveNames);
    }else{
        ui->showTutorMoves->setText("Show tutor moves");
        ui->label->setText("TMs/HMs");
        ui->tmList->addItems(tmNames);
    }

    ui->tmList->setCurrentRow(0);
    updateCheckboxes();
}


bool MainWindow::getLearnset(int moveId, int monId){
    if(showTutorMoves){
        return tutorLearnset[moveId][monId];
    }
    return learnset[moveId][monId];
}

void MainWindow::setLearnset(int moveId, int monId, bool value){
    if(showTutorMoves){
        tutorLearnset[moveId][monId] = value;
    }else{
        learnset[moveId][monId] = value;
    }
}
