# Decomp TM/HM Learnset Editor
A simple graphical tool for editing TM/HM/Tutor learnsets in the [pokeemerald decompilation](https://github.com/pret/pokeemerald).

![](https://gitlab.com/Fexean/decomp-tmhm-editor/-/raw/master/screenshot.png)

## Building
Open .pro file in Qt Creator and compile.

Alternatively you can compile from the command line:

    qmake
    make
